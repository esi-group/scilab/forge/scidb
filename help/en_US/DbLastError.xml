<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Copyright (C) Igor GRIDCHYN
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution. The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 -->

<refentry xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook"
          version="5.0-subset Scilab"
          xml:lang="en"
          xml:id="DbLastError">

  <refnamediv>
    <refname>DbLastError</refname>
    <refpurpose>get the last error, encountered during database interaction</refpurpose>
  </refnamediv>

  <!-- ===================================================================== -->
  <!-- Calling Sequence -->
  <!-- ===================================================================== -->
  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>
      DbQuery(query)
      DbQuery(dblink, query)
    </synopsis>
  </refsynopsisdiv>

  <!-- ===================================================================== -->
  <!-- Parameters -->
  <!-- ===================================================================== -->

  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>dblink</term>
        <listitem>
          <para>
          a pointer to database connection object (returned by DbConnect())
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>query</term>
        <listitem>
          <para>
          a string value of query executed
          </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <!-- ===================================================================== -->
  <!-- Description -->
  <!-- ===================================================================== -->

  <refsection>
    <title>Description</title>
    <para>
      <literal>
        DbQuery()
      </literal>
      executes a query on default connection and returns a pointer to result handler or results in an error if a query cannot be executed.
    </para>
    <para>
      <literal>
        DbDisconnect(dblink)
      </literal>
      executes a query on previously opened connection, represented by dblink pointer and returns a pointer to result handler
      or results in an error if a query cannot be executed.
    </para>
  </refsection>

  <!-- ===================================================================== -->
  <!-- Examples -->
  <!-- ===================================================================== -->

  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[
// Opens a “PostgreSQL” connection with a connexion string
dblink = DbConnect("PSQL", ..
"Server=127.0.0.1;Database=myDataBase;User Id=myUsername;Password=myPassword;");

resultHandle = DbQuery(dblink, "SELECT * FROM data_table");

results = DbFetchStruct(resulthandle);

//Disconnect from default (the last) database myDataBase
DbDisconnect();
  ]]></programlisting>
  </refsection>


  <!-- ===================================================================== -->
  <!-- See also -->
  <!-- ===================================================================== -->

  <refsection>
    <title>See Also</title>
    <simplelist type="inline">
      <member>
        <link linkend="DbConnect">
          DbConnect
        </link>
      </member>
      <member>
        <link linkend="DbDisconnect">
          DbDisconnect
        </link>
      </member>
    </simplelist>
  </refsection>


  <!-- ===================================================================== -->
  <!-- Authors -->
  <!-- ===================================================================== -->

  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>Igor GRIDCHYN</member>
    </simplelist>
  </refsection>

</refentry>
