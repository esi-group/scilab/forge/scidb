/*
 * Copyright (C) 2010 - Igor GRIDCHYN
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution. The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#ifndef FUZZY_OBJECT_H
#define FUZZY_OBJECT_H

#include "QtIncludes.h"

class FSQL_EXPORT FuzzyObject
{
	int _columnId;
	int _fuzzyId;
	QString _fuzzyName;
	int _fuzzyType;

public:
	int columnId();
	int fuzzyId();
	QString fuzzyName();
	int fuzzyType();

	FuzzyObject(QSqlQuery qry);
	FuzzyObject(int columnId,
				int fuzzyId,
				QString fuzzyName,
				int fuzzyType);
	FuzzyObject(QMap<QString, QVariant> *mValues);
	FuzzyObject(void);
	~FuzzyObject(void);

	QMap<QString, QVariant>* ValuesMap();	
};

#endif // FUZZY_OBJECT_H