// Copyright (C) 2010 - Igor GRIDCHYN
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution. The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

database = get_absolute_file_path("DbFirst.dem.sce") + "addressbook";

disp("connStr = struct (""provider"", ""QSQLITE"", ""database"", """+database+""")");
connStr = struct ("provider", "QSQLITE", "database", database)
disp("Connecting to a database:");
disp("DbConnect(connStr)");
DbConnect(connStr)
disp("resultHandler = DbQuery(""SELECT * FROM addressbook"")");
resultHandler = DbQuery("SELECT * FROM addressbook");

disp("recordStruct = DbFetchStruct(resultHandler) //getting first result as struct");
recordStruct = DbFetchStruct(resultHandler);
disp(recordStruct, "recordStruct =");

disp("DbFirst(resultHandler) //setting result pointer to first result");
DbFirst(resultHandler);

disp("recordStrings = DbFetchString(resultHandler) //getting frist result as string");
recordStrings = DbFetchString(resultHandler);
disp(recordStrings ,"recordStrings=");


disp("DbDisconnect()");
DbDisconnect();
